// создаем объект
var app = app || {};

$(function () {
    'use strict';

    //объявляем класс
    app.MyObject = Backbone.Model.extend({

        defaults: {
            name: "ball",
            size: 100,
            desc: "mega ball"
        },

        initialize: function() {

        },

        validate: function(attrs) {
            if (!(attrs.size >= 0)) {
                return "incorrect size";
            }

            if (!(attrs.name.length > 3 && attrs.name.length < 24)) {
                return "incorrect name";
            }

            if (!(attrs.desc.length > 3 && attrs.desc.length < 24)) {
                return "incorrect description";
            }
        }

    });

    //объявляем коллекцию
    app.MyObjectCollection = Backbone.Collection.extend({
        model: app.MyObject
    });

});

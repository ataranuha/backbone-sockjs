// создаем объект
var app = app || {};

$(function () {
    'use strict';
    app.connection = {
        connect: function () {
            this.sock = new SockJS('http://127.0.0.1:8888/echo', null, {});
            this.sock.onopen = function () {
            console.log('open');
        };

        this.sock.onmessage = function (e) {
            //console.log('message', e.data);
            app.connection.inMessage('in message:'+e.data);
        };

        this.sock.onclose = function () {
            setTimeout(function () {
                console.log('reconnect');
                app.connection.connect();
            }, 2000);
            console.log('close');
        };
    },

    inMessage: function (data) {
        var msg = JSON.parse(data);
        app.connection.msg = msg;
        console.log(data);
        /*if (msg.type = 'sysInfo') {
            _app.chart.series[0].addPoint([msg.response.timestamp, msg.response.price]);
            $('#si_price').text(msg.response.price);
            $('#si_listeners').text(msg.response.listeners);
            var divs = $('#log').find('div');
            if (divs.length >9) {
                divs.first().remove();
            }
            var v = "down";
            if (msg.response.diff>0) {
                var v = "up";
            }
            $('#log').append(
                '<div class="log-row"><i class="icon-arrow-'+v+'"></i><b>'
                    +msg.response.username
                    +'</b> trade: ' + msg.response.price
                    +' dif: '+msg.response.diff
                    +'</div>');

        }*/
    }
};
});
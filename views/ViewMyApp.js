// создаем объект
var app = app || {};



$(function () {
    'use strict';




    app.MyAppView = Backbone.View.extend({
        el: "#myAppView",

        events: {
            "click .addObject": "addObject",
            "click .toJSON": "toJSON",
            "click .connect": "connect"
        },

        initialize: function() {
            this.coll = new app.MyObjectCollection();

            this.$list = this.$('.myObjectList');

            this.listenTo(this.coll, "change", this.render);
            this.listenTo(this.coll, 'add', this.addOne);

            //this.render();
        },

        render: function() {

        },

        addObject: function() {
            this.coll.add([{}]);
        },

        addOne: function (model) {
            var newView = new app.MyObjectView ({ model: model });
            this.$list.append(newView.render().el);
        },

        toJSON: function () {
            var json = this.coll.toJSON();
            console.log(JSON.stringify(json));
        },

        connect: function(event) {
            app.connection.connect();
            $(event.target).hide();
        }


    });

});

// создаем объект
var app = app || {};

$(function () {

    app.MyObjectView = Backbone.View.extend({
        template: _.template($('#myObject').html()),
        tagName: 'tr',

        events: {
            "click .changeSize":    "changeSize",
            "blur  .desc":          "editValue",
            "blur  .name":          "editValue",
            'blur  .size':          'editValue',
            "click .deleteRow":     "deleteRow"
        },

        initialize: function() {
            this.listenTo(this.model, "change", this.render);
            this.listenTo(this.model, 'destroy', this.remove);
            this.render();
        },

        render: function() {
            var view = this.template(this.model.toJSON());
            this.$el.html(view);
            this.$name = this.$('.name');
            this.$desc = this.$('.desc');
            this.$size = this.$('input.size');
            return this;
        },

        deleteRow: function() {
            this.model.destroy();
        },

        editValue: function (ev) {
            var ret = this.model.set({
                name: this.$name.text(),
                desc: this.$desc.text(),
                size: parseInt(this.$size.attr('value'))
            },{validate:true});

            if (!ret) this.render();
        },

        changeSize: function (ev) {
            var diff = parseInt($(ev.target).attr('data-rel'));
            var size = parseInt(this.model.get('size'));
            var ret = this.model.set({
                size: size+diff
            },{validate:true});

            if (!ret) this.render();
        }

    });

});
